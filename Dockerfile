# Dockerfile

FROM ubuntu:bionic

RUN apt-get update
RUN apt install -y git cmake ccache p7zip-full build-essential zlib1g libstdc++6 libunwind-dev
RUN rm -rf /var/lib/apt/lists/*
